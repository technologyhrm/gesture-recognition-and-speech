package com.hrm.wts.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hrm.wts.R;
import com.hrm.wts.util.Constants;
import com.hrm.wts.util.Util;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getName();

    private TextView addedWordsTxt;
    private EditText writeWordEdtTxt;
    private ImageButton addLineImgBtn;
    private Button speakBtn;
    private TextToSpeech textToSpeech;

    private InputMethodManager inputMethodManager;
    private SharedPreferences sharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(LOG_TAG, "onCreate(), initializing all views and objects");

        addedWordsTxt = (TextView) findViewById(R.id.addedWordsTxt);
        writeWordEdtTxt = (EditText) findViewById(R.id.writeWordEdtTxt);
        addLineImgBtn = (ImageButton) findViewById(R.id.addLineImgBtn);
        addLineImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTextToSpeakListAndClearEditText();
            }
        });
        speakBtn = (Button) findViewById(R.id.speakBtn);

        inputMethodManager = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                textToSpeech.setLanguage(Locale.ENGLISH);
                textToSpeech.setLanguage(new Locale(Constants.LOCALE_HINDI));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "Verify Google keyboard status");
        checkGoogleHandWritingKeyboard();
    }

    private void checkGoogleHandWritingKeyboard() {
        final List<InputMethodInfo> inputMethodInfoList = inputMethodManager.getInputMethodList();
        InputMethodInfo googleMethodInfo = null;
        for (InputMethodInfo inputMethodInfo : inputMethodInfoList) {
            if (Constants.GOOGLE_HAND_WRITING_KEYBOARD_ID.equals(inputMethodInfo.getId())) {
                googleMethodInfo = inputMethodInfo;
                break;
            }
        }
        if (googleMethodInfo == null) {
            Log.i(LOG_TAG, "Request Google handwriting keyboard download");
            requestGoogleHandwritingKeyboardDownload();
            return;
        }
        final List<InputMethodInfo> enabledInputMethodInfos = inputMethodManager.getEnabledInputMethodList();
        if (!enabledInputMethodInfos.contains(googleMethodInfo)) {
            Log.i(LOG_TAG, "Request Google handwriting keyboard enable");
            launchGoogleHandWritingIMESettingsToEnable();
            return;
        }
        selectGoogleHandWritingKeyboard();
    }

    private void selectGoogleHandWritingKeyboard() {
        Log.d(LOG_TAG, "Request to select Google handwriting keyboard from chooser");
        if (!Constants.SUB_TYPE_MODE.equals(inputMethodManager.getCurrentInputMethodSubtype().getMode())) {
            inputMethodManager.showInputMethodPicker();
            final Toast toast = Toast.makeText(MainActivity.this, "Select Google Handwriting keyboard", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private void requestGoogleHandwritingKeyboardDownload() {
        final AlertDialog.Builder builder = Util.getSimpleAlertDialogBuilder(this,
                getString(R.string.download_request), getString(R.string.request_description));
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Intent googleHandwritingIMEDownloadRequestIntent = new Intent(Intent.ACTION_VIEW);
                googleHandwritingIMEDownloadRequestIntent.setData(Uri.parse(Constants.GOOGLE_HAND_WRITING_IME_DOWNLOAD_URI));
                startActivity(googleHandwritingIMEDownloadRequestIntent);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                MainActivity.this.finish();
            }
        });
        builder.create().show();
    }

    private void launchGoogleHandWritingIMESettingsToEnable() {
        final PackageManager packageManager = MainActivity.this.getPackageManager();
        final Intent launchGoogleHandWritingIMESettings = packageManager.getLaunchIntentForPackage(Constants.GOOGLE_HAND_WRITING_KEYBOARD_PACKAGE);
        launchGoogleHandWritingIMESettings.addCategory(Intent.CATEGORY_LAUNCHER);
        launchGoogleHandWritingIMESettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(launchGoogleHandWritingIMESettings);
        Toast.makeText(MainActivity.this, "Please enable Google Hand Writing keyboard", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final boolean speakPrefs = sharedPrefs.getBoolean(getString(R.string.speak_prefs), false);
        menu.findItem(R.id.speakPrefs).setChecked(speakPrefs);
        updateUIPreference(speakPrefs);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.speakPrefs == item.getItemId()) {
            if (item.isChecked()) {
                item.setChecked(false);
                sharedPrefs.edit().putBoolean(getString(R.string.speak_prefs), false).apply();
                updateUIPreference(false);
            } else {
                item.setChecked(true);
                sharedPrefs.edit().putBoolean(getString(R.string.speak_prefs), true).apply();
                updateUIPreference(true);
            }
            invalidateOptionsMenu();
            return true;
        }
        return false;
    }

    private void updateUIPreference(boolean isSentenceSpeak) {
        writeWordEdtTxt.setText("");
        addedWordsTxt.setText("");
        if (isSentenceSpeak) {
            addLineImgBtn.setVisibility(View.VISIBLE);
            writeWordEdtTxt.setFilters(new InputFilter[]{Util.getLineFilter(new Util.OnLineFeed() {
                @Override
                public void detected() {
                    addTextToSpeakListAndClearEditText();
                }
            })});
        } else {
            addLineImgBtn.setVisibility(View.GONE);
            writeWordEdtTxt.setFilters(new InputFilter[]{Util.getSpaceFilter(), Util.getLineFilter(new Util.OnLineFeed() {
                @Override
                public void detected() {
                    addTextToSpeakListAndClearEditText();
                }
            })});
        }
    }

    private void addTextToSpeakListAndClearEditText() {
        final String addWord = writeWordEdtTxt.getText().toString();
        if (addWord.trim().isEmpty())
            return;
        addedWordsTxt.append(addWord);
        addedWordsTxt.append(Character.toString(Constants.LINE_FEED));
        writeWordEdtTxt.setText("");
    }

    public void onButtonClick(View view) {
        final Button button = (Button) view;
        if (getString(R.string.speak).equals(button.getText().toString())) {
            final boolean speakPrefs = sharedPrefs.getBoolean(getString(R.string.speak_prefs), false);
            final String stringsToSpeak = addedWordsTxt.getText().toString();
            if (stringsToSpeak.trim().isEmpty())
                return;
            if (speakPrefs) {
                Util.speakSentenceBySentence(textToSpeech, stringsToSpeak, utteranceProgressListener);
            } else {
                Util.speakWordByWord(textToSpeech, stringsToSpeak, utteranceProgressListener);
            }
            button.setText(getString(R.string.stop_speak));
        } else if (getString(R.string.reset).equals(button.getText().toString())) {
            addedWordsTxt.setText("");
            writeWordEdtTxt.setText("");
        } else if (getString(R.string.stop_speak).equals(button.getText().toString())) {
            textToSpeech.stop();
            button.setText(getString(R.string.speak));
        }
    }

    private final UtteranceProgressListener utteranceProgressListener = new UtteranceProgressListener() {
        @Override
        public void onStart(String utteranceId) {
            Log.d(LOG_TAG, "started speaking");
        }

        @Override
        public void onDone(String utteranceId) {
            Log.d(LOG_TAG, "Finished speaking");
            speakBtn.setText(getString(R.string.speak));
        }

        @Override
        public void onError(String utteranceId) {
            Log.e(LOG_TAG, "Error speaking");
        }
    };
}
