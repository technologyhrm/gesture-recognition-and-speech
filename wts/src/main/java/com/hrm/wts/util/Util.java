package com.hrm.wts.util;


import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.Spanned;

public class Util {

    private Util() {
    }

    public interface OnLineFeed {
        void detected();
    }

    public static AlertDialog.Builder getSimpleAlertDialogBuilder(@NonNull Context context, @NonNull String title, @NonNull String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        return builder;
    }

    public static InputFilter getSpaceFilter() {
        return new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isSpaceChar(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };
    }

    public static InputFilter getLineFilter(final OnLineFeed onLineFeed) {
        return new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Constants.LINE_FEED == (source.charAt(i))) {
                        onLineFeed.detected();
                    }
                }
                return null;
            }
        };
    }

    @SuppressWarnings("deprecation")
    public static void speakWordByWord(TextToSpeech textToSpeech, String stringsToSpeak, UtteranceProgressListener listener) {
        final String[] speakWords = stringsToSpeak.split("\\n");
        for (String speakWord : speakWords) {
            final String wordToSpeak = speakWord.charAt(0) + " for " + speakWord;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textToSpeech.speak(wordToSpeak, TextToSpeech.QUEUE_ADD, null, TextToSpeech.ACTION_TTS_QUEUE_PROCESSING_COMPLETED);
            } else {
                textToSpeech.speak(wordToSpeak, TextToSpeech.QUEUE_ADD, null);
            }
        }
        textToSpeech.setOnUtteranceProgressListener(listener);
    }


    @SuppressWarnings("deprecation")
    public static void speakSentenceBySentence(TextToSpeech textToSpeech, String stringsToSpeak, UtteranceProgressListener listener) {
        final String[] speakWords = stringsToSpeak.split("\\n");
        for (String speakWord : speakWords) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textToSpeech.speak(speakWord, TextToSpeech.QUEUE_ADD, null, TextToSpeech.ACTION_TTS_QUEUE_PROCESSING_COMPLETED);
            } else {
                textToSpeech.speak(speakWord, TextToSpeech.QUEUE_ADD, null);
            }
        }
        textToSpeech.setOnUtteranceProgressListener(listener);
    }
}
