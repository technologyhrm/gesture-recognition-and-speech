package com.hrm.wts.util;


public class Constants {

    private Constants() {
    }

    /**
     * Google hand writing ime ID used to verify enabled status.
     */
    public static final String GOOGLE_HAND_WRITING_KEYBOARD_ID = "com.google.android.apps.handwriting.ime/.HandwritingIME";

    /**
     *
     */
    public static final String GOOGLE_HAND_WRITING_KEYBOARD_PACKAGE = "com.google.android.apps.handwriting.ime";

    /**
     *
     */
    public static final String GOOGLE_HAND_WRITING_SETTINGS = "com.google.android.apps.handwriting.ime.HandwritingSettings";

    /**
     *
     */
    public static final String GOOGLE_HAND_WRITING_IME_DOWNLOAD_URI = "market://details?id=" + GOOGLE_HAND_WRITING_KEYBOARD_PACKAGE;

    /**
     *
     */
    public static final String SUB_TYPE_MODE = "handwriting";
    /**
     *
     */
    public static final char LINE_FEED = '\n';

    /**
     *
     */
    public static final String LOCALE_HINDI = "hi";

}
